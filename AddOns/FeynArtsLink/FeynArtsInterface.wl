(* ::Package:: *)

(* ::Section::Closed:: *)
(*Begin*)


InclusiveQCDProcesses;


FeynArtsCreateAmp::usage="FeynArtsCreateAmp[dir,processInf] with 'processInf'=<| \"Process\"\[Rule]\"process\", \
\"LoopOrder\"->loopOrder, \"FeynArtsOptions\"\[Rule]opts |> generates Feynman amplitude for the process \
\"process\" at 'loopOrder' loop(s) using FeynArts. 
\"process\" looks like \"e+ e- -> u u~ g\" or \"-F[2,{1}] F[2,{1}] -> u u~ V[5]\" (a string separated by spaces, \
means \!\(\*SuperscriptBox[\(e\), \(+\)]\)\!\(\*SuperscriptBox[\(e\), \(-\)]\)->\
u \!\(\*OverscriptBox[\(u\), \(_\)]\) g). \
See FeynArtsParticles to figure out all shorthands of particles.
'opts' looks like \[LeftAssociation]\"InsertFields\"->\"{ExcludeParticles\[Rule]{_S*(_:1)}}\", \
\"Paint\"->\"AutoEdit->False\"\[RightAssociation], which sets options of functions in FeynArts.
All generated files will be stored in the directory 'dir'.
OptionValue[\"TemplateFileName\"] gives the path to the FeynArts template file, which can be either \
the template provided by the package or a template defined by user.";


FeynArtsParticles::usage="FeynArtsParticles[mod] gives shorthands of particles in the model 'mod'.
FeynArtsParticles[] gives the list of models which have been assigned shorthands.
User can also define his/her own shorthands for any model."


FeynArtsReadAmp::usage="FeynArtsReadAmp[filename] reads Feynman amplitudes \
in the file 'filename' generated by FeynArts.
The options \"ExternalMomentumName\" and \"LorentzIndexName\", \
stand for names of external momenta and Lorentz indexes, respectively, \
and {qc, gc}=OptionValue[\"SUNIndexName\"] stand for names of color indexes of quarks and gluons.";


Begin["`Private`"];

End[];


Begin["`FeynArts`"];


(* ::Section:: *)
(*FeynArtsCreateAmp*)


Options[FeynArtsCreateAmp]={"TemplateFileName":>FileNameJoin[{CalcLoopOptions["CalcLoopLocation"]//DirectoryName,
	"AddOns","FeynArtsLink","FeynArtsTemplate.m"}]};


FeynArtsCreateAmp[dir_,conf_,OptionsPattern[]]:=Module[
	{feynarts,current,runfile,mod,conf2,log,logfile,str,process},
	
	feynarts=CalcLoopOptions["FeynArtsLocation"];
	If[FindFile[feynarts]===$Failed,
		Print["Cannot find FeynArts package. Please \
provide the path of FeynArts package by setting CalcLoopOptions[\"FeynArtsLocation\"]."];
		Abort[]
	];
	
	current=FileNameJoin[{dir,"FeynArtsLog"}];
	If[FileNames[current]==={},CreateDirectory@current];
	runfile=FileNameJoin[{current,"runFeynArts.wl"}];
	
	If[FileExistsQ[runfile],DeleteFile[runfile]];
	str=ReadString[OptionValue["TemplateFileName"]];
	WriteString[runfile,"feynarts="<>writeFileName[feynarts]<>";\n"<>"dir="<>writeFileName[current]<>";\n"<>str];
	Close[runfile];
	
	mod="Model"/.ReadStringAndSymbol[Lookup[conf["Options"],"InsertFields","{}"],Identity];
	(*Default value is "SMQCD"*)
	mod=mod/."Model"->"SMQCD";
	
	conf2=conf;
	process=StringSplit[conf2["Process"]];
	conf2["LoopOrder"]=process[[-2]]//ToExpression;
	conf2["Process"]=processInterpretation[StringRiffle@process[[1;;-3]],mod];
	WriteMessage["*** Process=",conf2["Process"],conf2["LoopOrder"]];
	Put[conf2,FileNameJoin[{current,"config.wl"}]];
	
	log=CalcLoopOptions["RunProcess"][runfile];
	If[log@"StandardOutput"==="Cannot find FeynArts.\n",
		Print["Fatal error: Cannot find FeynArts!"];
		Abort[]
	];
	
	log=StringRiffle[If[Head@#=!=String,ToString@#,#]&/@Values[log],
					"\n********************************************\n"];
	logfile=FileNameJoin[{current,"FeynArts.log"}];
	If[FileExistsQ[logfile],DeleteFile[logfile]];
	WriteString[logfile,log];
	Close[logfile];
];


(* ::Section::Closed:: *)
(*FeynArtsReadAmp*)


Attributes[FeynArtsReadAmp]={Listable};
Options[FeynArtsReadAmp]={"ExternalMomentumName"->"p",
	"LorentzIndexName"->"mu","SUNIndexName"->{"qc","gc"}};
FeynArtsReadAmp[filename_,OptionsPattern[]]:=Module[
	{ext,mu,qc,gc,in,out,res,nin,FA},
	
	(*ToExpression does not work if "\r\n" presents at the end of a string.*)
	res=StringReplace[ReadString[filename],"\r\n"->"\n"];
	
	(*Replace symbols in FeynArts by strings*)
	res=ReadStringAndSymbol[res,FA]/.
		{FA["I"]->I,FA["Sqrt"]->Sqrt,FA["Conjugate"]->Conjugate};

	(*remove useless information*)
	res=res/.FA["FeynAmpList"][rule_,___][x___]:>{rule[[2]],{x}}/.FA["FeynAmp"][_,_,x_]:>x/.
		{FA["PolarizationVector"][_,k_,mu_]:>PolarizationVector[{k,1},LorentzIndex@mu], 
			Conjugate[FA["PolarizationVector"]][_,k_,mu_]:>PolarizationVector[{k,-1},LorentzIndex@mu]};

	(*(1\[PlusMinus]\[Gamma]^5)/2*)
	res=res/.FA["NonCommutative"][a___,FA["ChiralityProjector"][sgn_],b___]:>
			1/2 FA["NonCommutative"][a,b]+sgn/2 FA["NonCommutative"][a,DiracGamma[5],b]/.
			FA["NonCommutative"][a___]:>Expand//@FA["NonCommutative"][a]/.
			FA["MatrixTrace"][a___]:>Expand//@FA["MatrixTrace"][a];

	res=res/.FA["DiracSpinor"][p_,m_]:>DiracSpinor[If[Head@p===Times,{-p,-1},{p,1}],m]/.
		FA["MatrixTrace"][x___]:>DiracChain[0,x,0]/.FA["NonCommutative"]:>DiracChain;
	
	res=res/.{FA["FourMomentum"][FA["Incoming"],i_]:>in[i],
				FA["FourMomentum"][FA["Outgoing"],i_]:>out[i],
				FA["FourMomentum"][FA["Internal"],i_]:>LoopMomentum[i]}/.
			  {FA["Index"][FA["Colour"],x_]:>qc[x],FA["Index"][FA["Gluon"],x_]:>gc[x],
			  FA["Index"][FA["Lorentz"],x_]:>mu[x]};

	res=res/.{FA["SUNT"][a_,i_,j_]:>SUNT[i,a,j],x:FA["IndexDelta"][_qc,_qc]:>SUNT@@x,
		x:FA["IndexDelta"][_gc,_gc]:>SUNDelta@@x,FA["SUNF"]->SUNF}/.
		{FA["DiracMatrix"]:>GA,FA["DiracSlash"]:>GS,FA["MetricTensor"]:>MT,FA["FourVector"]:>LV}/.
		FA["ScalarProduct"]:>SP;
	
	res=res/.FA["FermionChain"]->DiracChain//.
		DiracChain[a__]:>DiracChain@@({a}/.DiracChain[b_]:>b)/.
		{FA["FeynAmpDenominator"]->Times, FA["PropagatorDenominator"][p_,m_,n_:1]:>GPD[{p,p,-m^2,1}]^n};
	
	res=res//.fac_*FA["SumOver"][index_gc|index_qc,__]/;!FreeQ[fac,index]:>fac/.
		{FA["SumOver"][index_qc,3]:>SUNN, FA["SumOver"][index_gc,8]:>SUNN^2-1};
	
	ext=OptionValue["ExternalMomentumName"];
	mu=OptionValue["LorentzIndexName"];
	{qc,gc}=#/*SUNIndex&/@OptionValue["SUNIndexName"];
	
	nin=Length@res[[1,1]];
	
	res=res/.{in[i_]:>ext[i],out[i_]:>ext[i+nin]};
	
	FA=CalcLoopSymbol;
	
	res=res//CLForm
];


(* ::Section::Closed:: *)
(*FeynArtsParticles*)


FeynArtsParticles[]:=DownValues[FeynArtsParticles][[All,1]]/.\!\(TraditionalForm\`FeynArtsParticles\)->List//ReleaseHold//Flatten//Union;


(*Each pair {in, ex} denots a particle defined in FeynArts and in CalcLoop, respectively*)
FeynArtsParticles["QED"]={
{"F[1,{1}]","e-"},
{"-F[1,{1}]","e+"},
{"F[1,{2}]","\[Mu]-"},
{"-F[1,{2}]","\[Mu]+"},
{"F[1,{3}]","\[Tau]-"},
{"-F[1,{3}]","\[Tau]+"},

{"V[1]","\[Gamma]"}
};

FeynArtsParticles["SMQCD"]={{"S[1]","H"},
{"S[2]","G0"},
{"S[3]","G-"},
{"-S[3]","G+"},

{"F[1,{1}]","\[Nu]e"},
{"-F[1,{1}]","\[Nu]e~"},
{"F[1,{2}]","\[Nu]\[Mu]"},
{"-F[1,{2}]","\[Nu]\[Mu]~"},
{"F[1,{3}]","\[Nu]\[Tau]"},
{"-F[1,{3}]","\[Nu]\[Tau]~"},

{"F[2,{1}]","e-"},
{"-F[2,{1}]","e+"},
{"F[2,{2}]","\[Mu]-"},
{"-F[2,{2}]","\[Mu]+"},
{"F[2,{3}]","\[Tau]-"},
{"-F[2,{3}]","\[Tau]+"},

{"F[3,{1}]","u"},
{"-F[3,{1}]","u~"},
{"F[3,{2}]","c"},
{"-F[3,{2}]","c~"},
{"F[3,{3}]","t"},
{"-F[3,{3}]","t~"},

{"F[4,{1}]","d"},
{"-F[4,{1}]","d~"},
{"F[4,{2}]","s"},
{"-F[4,{2}]","s~"},
{"F[4,{3}]","b"},
{"-F[4,{3}]","b~"},

{"V[1]","\[Gamma]"},
{"V[2]","Z"},
{"V[3]","W-"},
{"-V[3]","W+"},
{"V[5]","g"},

{"U[1]","u\[Gamma]"},
{"-U[1]","u\[Gamma]~"},
{"U[2]","uZ"},
{"-U[2]","uZ~"},
{"U[3]","u-"},
{"U[4]","u+"},
{"U[5]","ug"},
{"-U[5]","ug~"}};


processInterpretation[pro0_,mod_]:=Module[
	{pro,nameTrans},
	pro=StringSplit/@StringSplit[pro0,"->"];
	If[Length@pro=!=2,
		Print["Unknown process: ",pro0];
		Abort[]
	];
	Do[nameTrans[pair[[2]]]=pair[[1]],{pair,FeynArtsParticles[mod]}];
	
	pro=pro[[1]]->pro[[2]]/.par_String:>nameTrans[par];
	(*If[!FreeQ[pro,nameTrans],
		Print["Unknown process: ",pro0];
		Abort[]
	];*)
	pro/.nameTrans->Identity
]


(* ::Section::Closed:: *)
(*InclusiveQCDProcesses*)


Options[InclusiveQCDProcesses]={"Nf"->3,"CounterTerm"->False,"LastSelection"->(True&)};


InclusiveQCDProcesses[LOprocess_,order_?IntegerQ,opts:OptionsPattern[]]:=
	InclusiveQCDProcesses[LOprocess,0;;order,opts];
InclusiveQCDProcesses[LOprocess0_,lo_;;order_,OptionsPattern[]]:=Module[
	{LOprocess,nf,q,quarks,g,G,in,out,n1,n2,lists,listq,processes,loopAndcounter,f,cases,str},
	
	nf=OptionValue["Nf"];
	
	LOprocess=StringSplit/@StringSplit[LOprocess0,"->"];
	{in,out}=LOprocess;
	{n1,n2}=Count[#,"X"]&/@{in,out};
	(*** each element of lists: {#q1, #q2, ..., #ghost, #gluon} ***)
	lists=Join@@Table[FrobeniusSolve[Join[ConstantArray[2,nf],{2,1}],n],{n,n1+n2+lo,n1+n2+order}];
	lists=Join@@Table[
		Permutations@Flatten@{
			Table[ConstantArray[{q[i],q[-i]},list[[i]]],{i,Length@list-2}],
			ConstantArray[{G[5],G[-5]},list[[-2]]],ConstantArray[g,list[[-1]]]
		}
		,{list,lists}
	];

	(*** move some elements to incoming particle list ***)
	lists={#[[1;;n1]]/.{q[i_]:>q[-i],G[i_]:>G[-i]},
		   #[[n1+1;;n1+n2]], Reverse@Sort@#[[n1+n2+1;;-1]]
		  }&/@lists;
	lists=DeleteDuplicates[lists];
		
	lists=ReplacePart[LOprocess[[1]],Position[LOprocess[[1]],"X"]->#[[1]]//Thread]->Join[
		ReplacePart[LOprocess[[2]],Position[LOprocess[[2]],"X"]->#[[2]]//Thread],#[[3]]
		]&/@lists;
	SortBy[lists,LeafCount];
	
	quarks=Association[1->"u",2->"d", 3->"s", 4->"c",5->"b",6->"t",
		-1->"u~",-2->"d~",-3->"s~",-4->"c~",-5->"b~",-6->"t~"
	];
	
	lists=lists/.{q->quarks,g->"g",G[5]->"ug",G[-5]->"ug~"};
	
	(*** first number: order of loops, second number: order of counter terms ***)
	loopAndcounter[n_]:=loopAndcounter[n]=Table[" "<>ToString@i<>" "<>ToString@j,{i,0,n},{j,0,n-i}]//Flatten;
	lists=Flatten@Table[
		cases=order+Length@out-Length@process[[2]]//loopAndcounter;
		str=StringJoin@@Join[process[[1]]/.x_String:>x<>" ",{"->"},process[[2]]/.x_String:>" "<>x];
		str<>#&/@cases,
		{process,lists}
	]/.{"List"->List,"Sequence"->Sequence};
	
	If[OptionValue["CounterTerm"]===False,
		lists=Select[lists,StringTake[#,-1]==="0"&]	
	];
	
	Select[lists,OptionValue["LastSelection"]]
];


(* ::Section::Closed:: *)
(*End*)


End[];
